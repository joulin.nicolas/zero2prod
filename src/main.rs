use std::net::SocketAddr;

use axum::{extract::Path, response::IntoResponse, routing::get, Router};

#[tokio::main]
async fn main() {
    let app = Router::new()
        .route("/", get(greet))
        .route("/:name", get(greet));

    let addr = SocketAddr::from(([127, 0, 0, 1], 3000));
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}

async fn greet(name: Option<Path<String>>) -> impl IntoResponse {
    let name = name
        .map(|value| value.to_string())
        .unwrap_or("World".to_string());
    format!("Hello {name} !")
}
